const express = require('express')
	, path = require('path')
	, passport = require('passport')
	, FacebookStrategy = require('passport-facebook').Strategy;

passport.use(new FacebookStrategy({
	clientID: '362759414139259',
	clientSecret: 'af0a6b78c20c0d8b51e7e58252b408d6',
	callbackURL: "http://127.0.0.1:9000/auth/facebook/callback"
}, (accessToken, refreshToken, profile, done) => {
	console.log(accessToken);
	console.log(refreshToken);
	console.log(profile);
	console.log(done);
}));

const app = express();

app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(path.resolve(__dirname, '..', 'build')));

app.get('/auth/facebook', passport.authenticate('facebook'));

app.get('/auth/facebook/callback',
	passport.authenticate('facebook', { successRedirect: '/',
										failureRedirect: '/login'
						})
);

app.get("*", (req, res) => {
	console.log('index.html');
	res.sendFile(path.resolve(__dirname, '..', 'build', 'index.html'));
});


module.exports = app;